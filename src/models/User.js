const { Schema, model } = require('mongoose');
const bcrypt = require('bcryptjs');

const userSchema = new Schema({
    username: String,
    email: String,
    password: String,

});

// Cifrado de Password

userSchema.methods.encryptPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password,salt); //devuelve el password encriptado
};

// Validacion de password

userSchema.methods.validatePassword = function (password) {
    return bcrypt.compare(password,this.password); // devuelve true o false 
};

module.exports = model('User',userSchema)
