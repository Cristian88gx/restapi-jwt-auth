const { Router } = require('express');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const config = require('../config');
const verifyToken = require('../controllers/verify-token');


const router = Router();

// Rutas

// Registrarse
router.post('/signup', async (req, res, next) => {
    try {
        const { username, email, password } = req.body;
        const user = new User({
            username: username,
            email: email,
            password: password
        });
        // Encripto el password
        user.password = await user.encryptPassword(user.password);
        // Guardo en la base de datos
        await user.save();
        console.log(user);
        // Genero el token de autenticacion
        const token = jwt.sign({ id: user._id }, config.secret, {
            expiresIn: 60 * 60
        });
        // Respuesta del servidor 
        res.json({
            auth: true,
            token: token
        });

    }
    catch (e) {
        console.log(e)
        res.status(500).send('There was a problem registering your user');
    }
});



// login
router.post('/signin', async (req, res, next) => {
    const { email, password } = req.body;
    // busco el usuario a partir de su email
    const user = await User.findOne({ email: email });
    if (!user) { // si no existe devuelvo un 404 y un mensaje
        return res.status(404).send("The email doesn't exists")
    }
    // valido el usuario
    const validPassword = await user.validatePassword(password);
    // Devuelvo la informacion
    if (!validPassword) {
        return res.status(401).send({ auth: false, token: null }); // password incorrecto
    }
    res.json('signin');
    // genero el token
    const token = jwt.sign({ id: user._id }, config.secret, {
        expiresIn: 60 * 60
    });
    // Devuelvo la respuesta del servidor 
    res.json({
        auth: true,
        token: token
    });
});

// Mi perfil
router.get('/profile', verifyToken, async (req, res, next) => {  // verifico el token
    // Devuelvo la informacion
    const user = await User.findById(req.Userid, { password: 0 });
    if (!user) { // controlo que el usuario este en la bd
        return res.status(404).send('No user found');
    }
    res.json(user);
});

// Cerrar sesion

router.get('/logout', function (req, res) {
    res.status(200).send({ auth: false, token: null });
});

module.exports = router;