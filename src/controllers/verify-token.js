// Este es un middleware que verifica si el token esta
const jwt = require('jasonwebtoken');
const config = require('../config');

function verifyToken(req, res, next) {
    // verifico que la peticion tenga el token
    const token = req.headers['x-access-token'];
    if (!token) {
        // Devuelvo un mensaje si no lo tiene
        return res.status(401).json({
            auth: false,
            message: 'No Token provided'
        });
    }
    // Verifico el token 
    const decoded = jwt.verify(token, config.secret);
    // guardo en req el id para compartir la informacion con todas las rutas
    req.userId = decoded.id;
    next();
}

module.exports = verifyToken;